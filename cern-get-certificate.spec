Name: cern-get-certificate
Version: 1.0.2
Release: 1%{?dist}
Summary: cern-get-certificate - utility to acquire host certificate(s) at CERN
Group: Applications/System
Source: %{name}-%{version}.tar.gz
License: GPL
Vendor: CERN
Packager: Linux.Support@cern.ch
URL: http://cern.ch/linux/

BuildRoot: %{_tmppath}/%{name}-%{version}-buildroot
BuildArch: noarch

Requires: cern-get-keytab >= 0.9.7
Requires: openssl
Requires: CERN-CA-certs >= 20120322-8.slc6
Requires: ca-certificates >= 2010.63-3.el6_1.5

%if 0%{?rhel} == 7
Requires: perl-Authen-Krb5 >= 1.7
BuildRequires: perl-generators
Requires: perl-XML-Simple
%else
Requires: python3-requests
Requires: python3-requests-kerberos
%endif
BuildRequires: pandoc

%description
%{name} is a CERN utility which retrieves from CERN Certification
Authority host certificates and stores these on local system.

Please note that this tool is CERN specific and of no
use outside CERN network.

%prep
%setup -q

%build
make build

%install
# Perl for 7
%if 0%{?rhel} == 7
make install_7 DESTDIR=$RPM_BUILD_ROOT prefix=%{_prefix}
# Python for 8 and 9
%else
make install DESTDIR=$RPM_BUILD_ROOT prefix=%{_prefix}
%endif


%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%{_sbindir}/cern-get-certificate
%{_mandir}/man3/cern-get-certificate.3*
%{_sysconfdir}/sysconfig/cern-get-certificate
%{_sysconfdir}/logrotate.d/cern-get-certificate
%{_sysconfdir}/cron.daily/cern-get-certificate

%doc README cern-get-certificate.example-sysconfig

%changelog
* Mon Oct 07 2024 Ben Morrice <ben.morrice@cern.ch< - 1.0.2-1
- Correct path of CA certs

* Thu Oct 12 2023 Ben Morrice <ben.morrice@cern.ch> - 1.0.1-1
- use pandoc instead of pod2man for man page generation

* Wed May 17 2023 Georgios Argyriou <georgios.argyriou@cern.ch> - 1.0.0-1
- cern-get-certificate ported to Python for 8 and 9
- Delay before 1st request is increased to 30 seconds for all builds (Perl and Python).

* Tue Dec 20 2022 Ben Morrice <ben.morrice@cern.ch> - 0.9.4-5
- explicitly define perl-XML-Simple as a Requires

* Fri Dec 16 2022 Ben Morrice <ben.morrice@cern.ch> - 0.9.4-4
- Fix build dependencies for 8 and 9

* Tue Apr 13 2021 Daniel Juarez <djuarezg@cern.ch> - 0.9.4-3
- Call to krb5ktauth missing hostname parameter

* Tue Nov 24 2020 Daniel Juarez <djuarezg@cern.ch> - 0.9.4-2
- Fix status check

* Fri Nov 20 2020 Daniel Juarez <djuarezg@cern.ch> - 0.9.4-1
- no need for header on cert verify

* Fri Apr 17 2020 Ben Morrice <ben.morrice@cern.ch> - 0.9.3-2
- add Requires for perl-Date-Manip, perl-Config-Simple

* Wed Nov 27 2019 Ben Morrice <ben.morrice@cern.ch> - 0.9.3-1
- fix "Possible precedence issue" on perl 5.20+ (el8)

* Thu Jun 07 2018 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 0.9.2
- add --hostname options.

* Wed May 04 2016 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 0.8.2
- fixed logrotate conf file permissions.

* Thu Apr 07 2016 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 0.8.1
- print ActivityID/RequestId on errors from CA.

* Tue Sep 01 2015 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 0.8
- perform krb5 auth for all operations.

* Mon Aug 24 2015 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 0.7.2
- fix subject for Grid certs.

* Fri Feb 27 2015 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 0.7.1
- fix handling of CA SOAP 'Denied by Policy Module' message

* Tue Feb 17 2015 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 0.7
- rewritten for new SOAP CA interface
- validity checked using OCSP

* Fri Feb 06 2015 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 0.6
- adding CERN Grid CA

* Thu Feb 05 2015 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 0.5-1
- usekrb5cfg option
- fetch and store CRL
- verify cert against CRL

* Tue Jan 20 2015 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 0.4-1
- use tmp. files
- add --status option

* Tue Dec 02 2014 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 0.2-1
- use cerndc.cern.ch/cernca.cern.ch for auth. verification.

* Mon Dec 01 2014 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 0.1-1
- initial working version.

* Wed Nov 26 2014 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 0.0.1-1
- initial test release.
