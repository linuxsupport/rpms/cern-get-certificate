% CERN-GET-CERTIFICATE(3)
% Jaroslaw Polok <jaroslaw.polok@cern.ch>, Ben Morrice <ben.morrice@cern.ch>
% October 2023

# NAME

cern-get-certificate - utility to acquire and store CERN Certification Authority and/or CERN Grid Certification Authority host certificate.

# DESCRIPTION

cern-get-certficate uses kerberos host identity to authenticate to CERN CA (Certification Authority)
and/or CERN Grid CA and request host certificate(s) for local system, then configures the system for autoenrollment
(automatic renewal of certificate). Installed cron job verifies certificate expiry date daily,
performs auto-renewal and optionally restarts service(s) using the certificate.
It also downloads Certificate Revocation List (CRL) and stores it in local files.

# SYNOPSIS

>     cern-get-certificate  --help
>
>     cern-get-certificate  --autoenroll | --noautoenroll | --renew
>                            [--verbose][--debug][--config CFGFILE]
>                            [--force][--usekrb5cfg][--cern | --grid][--hostname HOSTNAME]
>     cern-get-certificate --status [--verbose][--debug][--config CFGFILE]
>                            [--cern | --grid]
>

# OPTIONS

**\-\-help**

    Shows this help description

**\-\-autoenroll**

    Enable the autoenrollment (automatic renewal of certificate) then obtain
    a host certificate from CERN CA and store it in file.

**\-\-noautoenroll**

    Disable the autoenrollment (automatic renewal of certificate). Certificate
    is not removed but it remains valid until expiry date.

**\-\-renew**

    Renew the certificate. Checks the validity of certificate and requests a new
    one if it would expire in less than 7 (default) days from the time of check.

    (this option is used mainly in the cron job)

**\-\-status**

    Check current status of autoenrollment, certificate expiry and its existence.

**\-\-cern**

    Process CERN CA certificates (this is the default option, may be omitted)

**\-\-grid**

    Process CERN Grid CA certificates.

**\-\-force**

    Override default checks and perform the operation.

**\-\-config CFGFILE**

    Use alternate configuration file.
    Note: /etc/cron.daily/cern-get-certificate should be altered if non-default
    configuration file is being used.

**\-\-usekrb5cfg**

    Use temporary kerberos configuration file:

    /tmp/cgk.krb5.conf

    created by:

        cern-get-keytab --leavekrb5cfg

    for client initialization.

    This is to be used in order to guarantee that same Active Directory Domain
    Controller that cern-get-keytab used will be used, therefore eliminating the
    need to wait for (asynchronous) AD replication.

**--hostname HOSTNAME**

    Request certificate for HOSTNAME instead of default system hostname.
    This option is to be used ONLY when system has multiple interfaces / ip addresses
    allocated. A keytab containing entries for non-default HOSTNAME is required.
    (this can be obtained with cern-get-keytab)

    HOSTNAME should be specified as fully qualified hostname (XXX.cern.ch)
    This option will not function for DNS host aliases.

    If this option is used, it should be set also in the configuration file
    for future certificate renewal.

# CONFIG FILE

    Configuration file /etc/sysconfig/cern-get-certificate contains default
    settings:

**keypath** - default: /etc/pki/tls/private/

        Private path to store certificate key in.

**certpath** - default: /etc/pki/tls/certs/

        Path to store certificate file in.

**days** - default: 7

        If validity of obtained certificate is shorter than defined number of days a new certificate is requested.

**uid** - default: 0 (root)
**gid** - default: 0 (root)

        Certificate files are chown'ed to be owned by designated user on the system.

**autorenew** - default: 1

        Perform automatic certificate renewal.

**autorenewexec** - default: none

        Command to be executed after a certificate
        renewal is performed.

        Example: "/sbin/service httpd reload"

**hostname** - default: none

        hostname for which certificate is to be obtained on multi interface systems.
        Use fully qualified hostname.

        Example: XXX.cern.ch

# EXAMPLES

>         cern-get-certificate --autoenroll
>         cern-get-certificate --autoenroll --grid
>         cern-get-certificate --noautoenroll
>         cern-get-certificate --renew
>         cern-get-certificate --status
>         cern-get-certificate --status --grid
>         cern-get-certificate --autoenroll --hostname XXX.cern.ch

# NOTES

NO E-MAIL EXPIRATION WANMINGS are sent for CERN/Grid host certificates
obtained using autoenrollment procedure

File names of stored certificate are:

key:

          'hostname.cern.ch.key'
        [ 'hostname.cern.ch.grid.key' ]

certificate:

          'hostname.cern.ch.pem'
          'hostname.cern.ch.crt'
        [ 'hostname.cern.ch.grid.pem' ]
        [ 'hostname.cern.ch.grid.crt' ]

(in \[\] names when --grid option is specified.)

# BUGS

?? (none reported ..)
